var gulp = require('gulp');

var templateCache = require('gulp-angular-templatecache');
var minifyHtml = require('gulp-minify-html');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var streamqueue = require('streamqueue');
var connect = require('gulp-connect');

gulp.task('minify', function() {
  var stream = streamqueue({objectMode: true});
  stream.queue(
              gulp.src('./src/*.html')
                  .pipe(minifyHtml({
                    empty: true,
                    spare: true,
                    quotes: true
                  }))
                  .pipe(templateCache({
                    module: 'schemaForm',
                    root: 'directives/decorators/bootstrap/fileUpload/'
                  }))
                  .pipe(connect.reload())
    );
  stream.queue(gulp.src('./src/*.js'));

  stream.done()
        .pipe(concat('dist/file-upload.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('.'));
});

gulp.task('non-minified-dist', function() {
  var stream = streamqueue({objectMode: true});
  stream.queue(
              gulp.src('./src/*.html')
                  .pipe(templateCache({
                    module: 'schemaForm',
                    root: 'directives/decorators/bootstrap/fileUpload/'
                  }))
                  .pipe(connect.reload())
    );
  stream.queue(gulp.src('./src/*.js'));

  stream.done()
        .pipe(concat('dist/file-upload.js'))
        .pipe(gulp.dest('.'));

});

gulp.task('connect', function() {
    connect.server({
        livereload: true,
        port: 9000
    });
});

gulp.task('watch', function() {
  gulp.watch('./src/**/*', ['default']);
});

gulp.task('default', [
  'minify',
  'non-minified-dist',
  'connect'
]);

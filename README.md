angular-schema-file-reader
===================

Angular schema form file reader

---

## Install and deploy

Clone project:
```
git clone git@bitbucket.org:manuelrojas/angular-schema-file-reader.git
cd <project location>
```

Pull npm dependencies:
```
npm install
```

Pull bower dependencies:
```
bower install
```

Start local instance:
```
gulp
```    
---
angular.module('schemaForm').config(
['schemaFormProvider', 'schemaFormDecoratorsProvider', 'sfPathProvider',
  function (schemaFormProvider, schemaFormDecoratorsProvider, sfPathProvider) {
            var fileUpload = function (name, schema, options) {
                if (schema.type === 'string' && schema.format === 'file') {
                    var f = schemaFormProvider.stdFormObj(name, schema, options);
                    f.key = options.path;
                    f.type = 'fileUpload';
                    options.lookup[sfPathProvider.stringify(options.path)] = f;
                    return f;
                }
            };

            schemaFormProvider.defaults.string.unshift(fileUpload);

            schemaFormDecoratorsProvider.addMapping(
                'bootstrapDecorator',
                'fileUpload',
                'directives/decorators/bootstrap/fileUpload/file-upload.html'
            );

            schemaFormDecoratorsProvider.createDirective(
                'fileUpload',
                'directives/decorators/bootstrap/fileUpload/file-upload.html'
            );
  }]);

angular.module('schemaForm').controller('fileReaderCtrl', function ($scope) {
  $scope.showContent = function($fileContent){
      $scope.content = $fileContent;
  };
});

angular.module('schemaForm').directive('onReadFile', function ($parse) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element, attrs, ngModelCtrl) {
          var fn = $parse(attrs.onReadFile);

          element.on('change', function(onChangeEvent) {
              var reader = new FileReader();

              reader.onload = function(onLoadEvent) {
                  scope.$apply(function() {
                      fn(scope, {$fileContent:onLoadEvent.target.result});
                  });
              };
              reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
          });
        }
    };
});

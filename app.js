var app = angular.module('app', ['schemaForm'])
.controller('UploadController', function ($scope, $rootScope) {
    'use strict';
    $scope.schema = {
        type: 'object',
        title: 'Upload',
        properties: {
            "filereader": {
                "title": 'File',
                "type": 'string',
                "format": 'file',
            }
        }
    };
    $scope.model = {};
    $scope.form = [
        {
            "key": "filereader",
            "type": "fileUpload",
        },
    ];
});
